package gato;

import javax.swing.JFrame;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JOptionPane;

/**
 * @author Veyda Alcalá Camacho             14300011
 * @author Astrid Danae Andalón Ramírez     14300021
 * @author Erick Jesús Castelao Alférez     14300072
 * @author Yael Arturo Chavoya Andalón      14300094
 * @author Maritza Hernández Contreras      14300191
 */
public class Bigotes extends JFrame {

    protected JButton[] buttons;
    private final JButton uno = new JButton(" "), dos = new JButton(" "),
            tres = new JButton(" "), cuatro = new JButton(" "),
            cinco = new JButton(" "), seis = new JButton(" "),
            siete = new JButton(" "), ocho = new JButton(" "),
            nueve = new JButton(" ");

    private Listener mListener;

    public interface Listener {

        void onClick(int number);
    }

    public Bigotes(Listener listener) {
        estilo();
        orden();
        botonesE();

        mListener = listener;
    }

    public void setIcons(String[] array) {
        setButton(uno, array[1]);
        setButton(dos, array[2]);
        setButton(tres, array[3]);
        setButton(cuatro, array[4]);
        setButton(cinco, array[5]);
        setButton(seis, array[6]);
        setButton(siete, array[7]);
        setButton(ocho, array[8]);
        setButton(nueve, array[9]);
    }
    
    public void lock(){
        uno.setEnabled(false);
        dos.setEnabled(false);
        tres.setEnabled(false);
        cuatro.setEnabled(false);
        cinco.setEnabled(false);
        seis.setEnabled(false);
        siete.setEnabled(false);
        ocho.setEnabled(false);
        nueve.setEnabled(false);
        this.refresh();
    }

    private void setButton(JButton button, String string) {
        button.setText(string);
        if (!string.equals(" ")) {
            button.setEnabled(false);
        } else {
            button.setEnabled(true);
        }
    }

    public void sendMessage(int what) {
        String x = "";
        switch (what) {
            case Estambre.WIN:
                x = "Ganaste";
                break;
            case Estambre.LOSE:
                x = "Perdiste";
                break;
            case Estambre.TIE:
                x = "Empate";
                break;
        }
        JOptionPane.showMessageDialog(this, x);
    }
    
    public void setButtonReady(int which, String what){
        switch(which){
            case 1: uno.setText(what); break;
            case 2: dos.setText(what); break;
            case 3: tres.setText(what); break;
            case 4: cuatro.setText(what); break;
            case 5: cinco.setText(what); break;
            case 6: seis.setText(what); break;
            case 7: siete.setText(what); break;
            case 8: ocho.setText(what); break;
            case 9: nueve.setText(what); break;
        }
        this.refresh();
    }

    ActionListener onButtonClick = (ActionEvent e) -> {
        if (e.getSource().equals(uno)) {
            mListener.onClick(1);
        }
        if (e.getSource().equals(dos)) {
            mListener.onClick(2);
        }
        if (e.getSource().equals(tres)) {
            mListener.onClick(3);
        }
        if (e.getSource().equals(cuatro)) {
            mListener.onClick(4);
        }
        if (e.getSource().equals(cinco)) {
            mListener.onClick(5);
        }
        if (e.getSource().equals(seis)) {
            mListener.onClick(6);
        }
        if (e.getSource().equals(siete)) {
            mListener.onClick(7);
        }
        if (e.getSource().equals(ocho)) {
            mListener.onClick(8);
        }
        if (e.getSource().equals(nueve)) {
            mListener.onClick(9);
        }
    };
    
    void refresh(){
        for(Component c : this.getComponents()){
            c.validate();
        }
        this.validate();
    }

    private void botonesE() {
        uno.addActionListener(onButtonClick);
        dos.addActionListener(onButtonClick);
        tres.addActionListener(onButtonClick);
        cuatro.addActionListener(onButtonClick);
        cinco.addActionListener(onButtonClick);
        seis.addActionListener(onButtonClick);
        siete.addActionListener(onButtonClick);
        ocho.addActionListener(onButtonClick);
        nueve.addActionListener(onButtonClick);
        
        int fontSize = 40;
        
        uno.setFont(new Font("Consolas", Font.PLAIN, fontSize));
        dos.setFont(new Font("Consolas", Font.PLAIN, fontSize));
        tres.setFont(new Font("Consolas", Font.PLAIN, fontSize));
        cuatro.setFont(new Font("Consolas", Font.PLAIN, fontSize));
        cinco.setFont(new Font("Consolas", Font.PLAIN, fontSize));
        seis.setFont(new Font("Consolas", Font.PLAIN, fontSize));
        siete.setFont(new Font("Consolas", Font.PLAIN, fontSize));
        ocho.setFont(new Font("Consolas", Font.PLAIN, fontSize));
        nueve.setFont(new Font("Consolas", Font.PLAIN, fontSize));       

        this.getContentPane().add(uno);
        this.getContentPane().add(dos);
        this.getContentPane().add(tres);
        this.getContentPane().add(cuatro);
        this.getContentPane().add(cinco);
        this.getContentPane().add(seis);
        this.getContentPane().add(siete);
        this.getContentPane().add(ocho);
        this.getContentPane().add(nueve);
    }

    public void orden() {
        int size = 60;
        GroupLayout orden = new GroupLayout(this.getContentPane());
        orden.setAutoCreateContainerGaps(true);
        orden.setAutoCreateGaps(true);
        orden.setHorizontalGroup(
                orden.createSequentialGroup()
                        .addGroup(
                                orden.createParallelGroup()
                                        .addComponent(uno, size, size, size)
                                        .addComponent(dos, size, size, size)
                                        .addComponent(tres, size, size, size)
                        )
                        .addGroup(
                                orden.createParallelGroup()
                                        .addComponent(cuatro, size, size, size)
                                        .addComponent(cinco, size, size, size)
                                        .addComponent(seis, size, size, size)
                        )
                        .addGroup(
                                orden.createParallelGroup()
                                        .addComponent(siete, size, size, size)
                                        .addComponent(ocho, size, size, size)
                                        .addComponent(nueve, size, size, size)
                        )
        );
        orden.setVerticalGroup(
                orden.createParallelGroup()
                        .addGroup(
                                orden.createSequentialGroup()
                                        .addComponent(uno, size, size, size)
                                        .addComponent(dos, size, size, size)
                                        .addComponent(tres, size, size, size)
                        )
                        .addGroup(
                                orden.createSequentialGroup()
                                        .addComponent(cuatro, size, size, size)
                                        .addComponent(cinco, size, size, size)
                                        .addComponent(seis, size, size, size)
                        )
                        .addGroup(
                                orden.createSequentialGroup()
                                        .addComponent(siete, size, size, size)
                                        .addComponent(ocho, size, size, size)
                                        .addComponent(nueve, size, size, size)
                        )
        );
        setLayout(orden);
        this.pack();
    }

    private void estilo() {//ventana
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Gato");
        Color otro = new Color(159, 105, 113);
        this.getContentPane().setBackground(otro);
        this.setLayout(null);
    }

}
