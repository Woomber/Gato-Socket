package gato;

/**
 * @author Veyda Alcalá Camacho             14300011
 * @author Astrid Danae Andalón Ramírez     14300021
 * @author Erick Jesús Castelao Alférez     14300072
 * @author Yael Arturo Chavoya Andalón      14300094
 * @author Maritza Hernández Contreras      14300191
 */
public class Gato {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Estambre juego = new Estambre();
        juego.play();
    }
    
}
