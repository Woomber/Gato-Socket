package gato;

/**
 * @author Veyda Alcalá Camacho 14300011
 * @author Astrid Danae Andalón Ramírez 14300021
 * @author Erick Jesús Castelao Alférez 14300072
 * @author Yael Arturo Chavoya Andalón 14300094
 * @author Maritza Hernández Contreras 14300191
 */
public class Estambre implements Bigotes.Listener {

    private static final boolean IS_SERVER = true;

    private Miau miau;
    private Bigotes bigotes;
    private boolean haGanado = false;
    private boolean firstTurn = true;
    private final String miSimbolo;

    public static final int WIN = 1;
    public static final int LOSE = 2;
    public static final int TIE = 3;

    public Estambre() {
        if (IS_SERVER) {
            miSimbolo = "X";
        } else {
            miSimbolo = "O";
        }
    }

    public void play() {

        if (IS_SERVER) {
            Patitas.despertar();
            miau = new Miau();
        } else {
            Patitas.despertarHijo();
        }

        bigotes = new Bigotes(this);
        bigotes.setVisible(true);
        bigotes.lock();

        check();
    }

    public void check() {
        if (haGanado) {
            Patitas.dormir();
            return;
        }
        if (firstTurn && IS_SERVER) {
            firstTurn = false;
        } else {
            String miCadena = Patitas.tragar();
            miau = new Miau(miCadena);
        }

        // Verificar si alguien ganó
        if (miau.maullar()) {
            Patitas.bolaDePelos(miau.tablero);
            switch (miau.tablero[0]) {
                case "X":
                case "O":
                    if (miSimbolo.equals(miau.tablero[0])) {
                        bigotes.sendMessage(WIN);
                    } else {
                        bigotes.sendMessage(LOSE);
                    }
                    break;
                case "E":
                    bigotes.sendMessage(TIE);
                    break;
            }
            haGanado = true;
            bigotes.setIcons(miau.tablero);
            bigotes.lock();
        } else {
            bigotes.setIcons(miau.tablero);
        }
    }

    @Override
    public void onClick(int number) {
        miau.tablero[number] = this.miSimbolo;
        bigotes.setButtonReady(number, this.miSimbolo);
        bigotes.lock();
        Patitas.bolaDePelos(miau.tablero);
        check();
    }

}
