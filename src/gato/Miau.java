package gato;

/**
 * @author Veyda Alcalá Camacho             14300011
 * @author Astrid Danae Andalón Ramírez     14300021
 * @author Erick Jesús Castelao Alférez     14300072
 * @author Yael Arturo Chavoya Andalón      14300094
 * @author Maritza Hernández Contreras      14300191
 */
public class Miau {

    public String[] tablero;

    public Miau() {
        tablero = new String[10];
        for(int i=0; i<10; i++){
            tablero[i] = " ";
        }
    }

    public Miau(String[] tablero) {
        this.tablero = tablero;
    }

    public Miau(String tablero) {
        this.tablero = tablero.split("(?!^)");
    }

    public boolean maullar() {
        int[][] pelos = new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}, {1, 4, 7}, {2, 5, 8}, {3, 6, 9}, {1, 5, 9}, {3, 5, 7}};
        for(int i=0; i<8; i++){
            if(!(tablero[pelos[i][0]].equals(" ")) &&
                tablero[pelos[i][0]].equals(tablero[pelos[i][1]]) &&
                tablero[pelos[i][0]].equals(tablero[pelos[i][2]])){
                    tablero[0] = tablero[pelos[i][0]];
                    return true;
            }
        }
        int spcCount = 0;
        for(int i = 1; i < tablero.length; i++){
            if(tablero[i].equals(" ")) spcCount++;
        }
        if(spcCount == 0){
            tablero[0] = "E";
            return true;
        }
        return false;
    }

    
    
}

