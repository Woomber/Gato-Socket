package gato;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

/**
 * @author Veyda Alcalá Camacho             14300011
 * @author Astrid Danae Andalón Ramírez     14300021
 * @author Erick Jesús Castelao Alférez     14300072
 * @author Yael Arturo Chavoya Andalón      14300094
 * @author Maritza Hernández Contreras      14300191
 */
public class Patitas {
    
    public static Socket conn;
    public static ServerSocket server;
    public static PrintWriter print;
    public static BufferedReader read;
    
    public static final String IP = "localhost";
    public static final int PORT = 95;
    
    public static void despertar(){
        try {
            server = new ServerSocket(PORT);
            conn = server.accept();
            print = new PrintWriter(conn.getOutputStream(), true);
            read = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        } catch(Exception ex){
            
        }          
    }
    
        public static void despertarHijo(){
        try {
            conn = new Socket(IP, PORT);
            print = new PrintWriter(conn.getOutputStream(), true);
            read = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        } catch(Exception ex){
            
        }            
    }
    
    public static void bolaDePelos(String[] pelos){
        StringBuilder bola = new StringBuilder("");
        for (String pelo : pelos) {
            bola.append(pelo);
        }
        print.println(bola.toString());        
    }
    
    public static String tragar(){
        byte[] arreglo = new byte[12];
        String cadena = "";
        try {
            while(cadena.length() != 10){
                 conn.getInputStream().read(arreglo);
                 cadena = new String(arreglo);
                 cadena = cadena.replace("\r\n", "");
            }
            return cadena;
        } catch(Exception ex){
            return null;
        }
       
    }
    
    public static void dormir(){
        try {
            server.close();
            conn.close();
            read.close();
            print.close();
            
        } catch(Exception ex){
            
        }
        
        
    }
    
}
